# start consul server
`consul agent -server --bootstrap-expect=1 -client=10.88.36.127 -data-dir=/storage1/consul -node=agent-one -bind=10.88.36.127 -config-dir=/etc/consul.d &`

#start consul agent
consul agent -client=10.88.36.126 -data-dir=/storage1/consul -bind=10.88.36.126 -retry-join=10.88.36.127 -config-dir=/etc/consul.d &



#docker start server
docker run -d --net=host consul agent -server -client=0.0.0.0 -ui -bootstrap-expect=1

#docker start agent
docker run -d --net=host consul agent -client=0.0.0.0 -ui -retry-join=10.88.36.127