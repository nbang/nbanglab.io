#!/bin/bash

#update packages
apt update && apt upgrade -y

#install some useful package - comment if unused
#build package
#apt-get install -y build-essential

#git package
apt install -y git
#nginx server
apt install -y nginx
#docker package 
apt install -y docker.io
#golang package
apt install -y golang

#nodejs
curl -sL https://deb.nodesource.com/setup_6.x | bash -
apt install -y nodejs

#install pm2
npm install -g pm2
pm2 install pm2-webshell
pm2 set pm2-webshell:username foo
pm2 set pm2-webshell:password bar
pm2 set pm2-webshell:bind 127.0.0.1

#install node-red
npm install -g node-red

#run node-red
pm2 start /usr/bin/node-red -- -v

#save and startup pm2
pm2 startup
pm2 save

#install influxdb
curl -sL https://repos.influxdata.com/influxdb.key | apt-key add -
source /etc/lsb-release
echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
apt update && apt install influxdb -y
service influxdb start
