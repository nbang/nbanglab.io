Assume OrangePI PC board is already up and run with armbian image
If not, read [README.md](README.md) first, quick-start session.

# Remote ssh command with default password (1234) - you will have to change password for first time usage
``` bash
    ssh root@orangepipc
```
# Copy ssh key:
``` bash
    ssh-copy-id root@orangepipc
```

# Copy and run [first_run.sh](src/first_run.sh) to install some debian useful package
``` bash
    scp first_run.sh root@OrangePI:~/first_run.sh
    chmod +x first_run.sh
    ./first_run.sh
```

# Command to get CPU information (temperature & usage percent)
``` bash
    cat /sys/devices/virtual/thermal/thermal_zone0/temp
    awk 'NR == 2 {print ($2+$4)*100/($2+$4+$5)}' /proc/stat
```

# Command to get memory information (usage percent)
``` bash
    free -m | awk 'NR == 2 {print $3*100/$2}'
```

# Edit /etc/modules to load gpio and w1 modules if needed
    gpio-sunxi
    wire
    w1-sunxi
    w1-gpio
    w1-therm

# Command to get Dallas 18DS20 temperature sensor
``` bash
    cat /sys/bus/w1/devices/28-00000501f786/w1_slave
```