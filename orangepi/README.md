## Place to buy the board:
    http://www.aliexpress.com/store/product/Orange-Pi-PC-ubuntu-linux-and-android-mini-PC-Beyond-and-Compatible-with-Raspberry-Pi-2/1553371_32448079125.html

## Orange PI home page:
    http://www.orangepi.org/

## Orange PI quick start:
    https://drive.google.com/file/d/0BwQT0FP7IkTbYmFleURSRkFNdlE

## Orange PI images download:
### Loboris linux images with kernel 3.4 - suggest images: debian jessie mini (with xfce if gui is needed)
    https://drive.google.com/folderview?id=0B1hyW7T0dqn6fndnZTRhRm5BaW4zVDVyTGlGMWJES3Z1eXVDQzI5R1lnV21oRHFsWnVwSEU&usp=sharing#list
    
### Armbian images:
    https://www.armbian.com/orange-pi-pc/
    https://image.armbian.com/nightly/Orangepipcplus_Ubuntu_xenial - for newest kernel (4.9)

## Burn image:
``` shell
diskutil unmountDisk disk2 && dd bs=1m if=filename.img of=/dev/rdisk2 && diskutil eject disk2
```
    
## Further reading: 
### My [Guideline.md](Guideline.md) to setup OrangePI PC with debian image.