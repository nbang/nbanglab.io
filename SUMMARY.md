# Summary

### Golang

* [CGo](golang/README.md)

### Java
* [Core](java/tips_trick/core.md)
* [Camel](java/camel_EIP.md)
* [Karaf](java/karaf_guideline.md)

### Linux
* [Shell](linux/shell.md)

### OrangePI
* [Quickstart](orangepi/README.md)
