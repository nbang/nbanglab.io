The cgo tool is enabled by default for native builds on systems where it is expected to work. It is disabled by default when cross-compiling. You can control this by setting the CGO_ENABLED environment variable when running the go tool: set it to 1 to enable the use of cgo, and to 0 to disable it. The go tool will set the build constraint "cgo" if cgo is enabled.

Useful link:
    http://devs.cloudimmunity.com/gotchas-and-common-mistakes-in-go-golang/index.html
    
Golang books:
    https://www.golang-book.com/
    
Run code forever: select{}