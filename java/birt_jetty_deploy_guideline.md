# BIRT viewer deployment with jetty
## Get lastest version of jetty and birt viewer
```
http://download.eclipse.org/birt/downloads/
http://www.eclipse.org/jetty/downloads.php
```
## Get oracle jdbc driver
Download ojdbc7.jar from oracle site
`http://www.oracle.com/technetwork/database/features/jdbc/jdbc-drivers-12c-download-1958347.html`
## Configure JNDI datasource for jetty
1. Adding following code into etc/jetty.xml
```
<!-- Oracle datasource setttings                                 -->
<New id="DataSource" class="org.eclipse.jetty.plus.jndi.Resource">
        <Arg></Arg>
        <Arg>jdbc/OracleDS</Arg>
        <Arg>
            <New class="oracle.jdbc.pool.OracleConnectionPoolDataSource">
            <Set name="URL">jdbc:oracle:thin:@10.88.36.47:1521:xe</Set>
            <Set name="User">system</Set>
            <Set name="Password">oracle</Set>
        </New>
    </Arg>
</New>
```
2. Copy ojdbc7.jar into lib/jndi folder
3. Extract birt viewer jetty webapps\birt folder
4. Add following code into webapps\birt\WEB-INF\web.xml
```
<resource-ref>
        <description>Resource reference to a factory for java.sql.Connection</description>
        <res-ref-name>jdbc/OracleDS</res-ref-name>
        <res-type>javax.sql.DataSource</res-type>
        <res-auth>Container</res-auth>
</resource-ref>
```
5. Copy report file *.rptdesign to webapps\birt folder
6. Edit report file add following code into <oda-data-source/> tag
```
<property name="odaJndiName">java:comp/env/jdbc/oracleDS</property>
```
  
## Setting true type fonts for birt pdf (only on linux based server)
Copy *.tff file into JRE_HOME\lib\fonts folder (ex: times.tff)