## String tips

1. Use String.equal() instead of ==
2. Use StringBuilder for String concatenations instead of + operator
3. Call .equals on known string constants rather than unknown variable
4. Use switch( ) instead of multiple if else-if
5. Use String.valueOf() instead of toString()
6. Create Strings as literals instead of creating String objects using 'new'
7. Use String.format() to format the the strings

## Check Oddity
```java
public boolean oddOrNot(int num) {
    return (num & 1) != 0;
}
