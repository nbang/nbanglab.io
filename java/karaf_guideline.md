# Download and install lastest karaf from apache site (4.0.8)
    wget http://mirror.downloadvn.com/apache/karaf/4.0.8/apache-karaf-4.0.8.tar.gz
    tar -xvf apache-karaf-4.0.8.tar.gz
    
# Start/stop karaf and connect using client 
    bin/start (use bin/start clean to reset karaf status)
    bin/stop
    bin/client -u karaf

# Install recommend module (depend on karaf version - use servicemix for reference)
    feature:repo-add hawtio 1.4.67
    feature:repo-add activemq 5.14.1
    feature:repo-add camel 2.18.1
    feature:install hawtio activemq-broker-noweb activemq-camel camel camel-cxf camel-xmljson camel-mongodb