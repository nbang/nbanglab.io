# Linux Shell
## Navigation
    cd 
    pwd

## Looking Around
    ls
    less
    file

## Manipulating Files
    cp - mv
    rm
    mkdir

## Working With Commands
	man
	type
	which
	help

## I/O Redirection
	>
	>>
	<
	|

## Filter Commands
	grep
	head - tail
	sort
	fmt
	uniq
	pr
	tr
	sed
	awk

## Permissions
	su
	sudo
	chmod
	chown
	chgrp

## Job Control
	ps
	kill
	jobs
	bg
	fg
	 &
	 
## Wildcards
    *
    ?
    [characters] - [:alnum:] - [:alpha:] - [:digit:] - [:upper:] - [:lower:]
    [!characters]

## Escape Characters
    \n
    \t
    \a
    \\
    \f